import pytest



#this test should pass as the substring is in the html file
def test1():
    f = open("myfile", "r")
    txt = f.read()
    assert "Right-click in the box below to see one called 'the-internet'" in txt
#this test should fail as the substring is not in the html file
def test2():
    f = open("myfile", "r")
    txt = f.read()
    assert "Alibaba" in txt, "Alibaba is not in the webpage"
